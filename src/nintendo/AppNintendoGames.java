package nintendo;

import java.util.List;

import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.BindingSet;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class AppNintendoGames extends Application {

	private Stage janelaMain;
	private Scene janelaListaDeFranquias;
	private Scene janalaDeDetalhesDoJogo;
	private ListView<String> listViewJogos = new ListView<>();
	private TableView<InformacoesDoJogo> tableViewJogos = criarTableView();

	public void start(Stage primaryStage) {
		janelaMain = primaryStage;

		/* Seta o titulo e icone do APP */
		janelaMain.setTitle("Nintendo Games ");
		Image image = new Image("/nintendo/nintendo.png");
		primaryStage.getIcons().add(image);

		preparaTelaPrincipal();
		preparaTelaDeDetalhesDoJogo();
		populaTelaInicial();

		/* Exibe janela inicial. */
		janelaMain.setScene(janelaListaDeFranquias);
		janelaMain.show();
	}

	private void populaTelaInicial() {
		/* Popula a lista de jogos */
		List<String> jogos = listViewJogos.getItems();
		for (BindingSet bs : ConsultasDBpedia.getSeriesNintendo())
			jogos.add(bs.getValue("Series").toString());
	}

	private void preparaTelaDeDetalhesDoJogo() {
		/* Botao para voltar para tela anterior a partir da tabela de detalhes */
		Button btnVoltar = new Button("Voltar");
		btnVoltar.setOnMouseClicked(e -> {
			janelaMain.setScene(janelaListaDeFranquias);
		});
		VBox telaDeDetalhes = new VBox(btnVoltar, tableViewJogos);
		janalaDeDetalhesDoJogo = new Scene(telaDeDetalhes, 400, 500);
	}

	private void preparaTelaPrincipal() {
		/* Determina a orientacao da tela de lista de jogos */
		listViewJogos.setOrientation(Orientation.VERTICAL);

		/*
		 * Trata o doubleClick nas celulas da lista de jogos.
		 */
		listViewJogos.setOnMouseClicked(click -> {
			if (click.getClickCount() == 2) {
				exibirInfoDoJogoSelecionado();
			}
		});

		/*
		 * Botao para selecionar uma serie e ir para a tabela de detalhes do jogo
		 */
		Button btnSelecionar = new Button("Selecionar");
		btnSelecionar.setOnMouseClicked(e -> exibirInfoDoJogoSelecionado());
		VBox container = new VBox(new Label("Selecione uma serie:"), listViewJogos, btnSelecionar);
		janelaListaDeFranquias = new Scene(container, 500, 500);
	}

	/*
	 * Extrai o valor de um binding set ou valor default, caso o jogo nao possua
	 * alguma informacao disponivel do DBPedia
	 */
	private String getValueOrDefault(BindingSet bindingSet, String chave, String valorDefault) {
		Value valor = bindingSet.getValue(chave);
		if (valor == null)
			return valorDefault;
		return valor.stringValue();
	}

	/* Carrega e exibe as informacoes dos jogos da Serie selecionada */
	private void exibirInfoDoJogoSelecionado() {
		ObservableList<InformacoesDoJogo> jogosNaTabela = buscarInformacoesJogoSelecionado();
		tableViewJogos.setItems(jogosNaTabela);
		janelaMain.setScene(janalaDeDetalhesDoJogo);
	}

	/* Busca as informacoes dos jogos da serie selecionada */
	private ObservableList<InformacoesDoJogo> buscarInformacoesJogoSelecionado() {
		String game = listViewJogos.getSelectionModel().getSelectedItem();
		List<BindingSet> informacoesDoJogo = ConsultasDBpedia.getInfoGame(game);
		ObservableList<InformacoesDoJogo> jogosNaTabela = FXCollections.observableArrayList();
		for (BindingSet bs : informacoesDoJogo) {
			String nome = getValueOrDefault(bs, "jogo", "Indisponivel");
			String genero = getValueOrDefault(bs, "genero", "Indisponivel");
			String modo = getValueOrDefault(bs, "modo", "Indisponivel");
			String plataforma = getValueOrDefault(bs, "plataforma", "Indisponivel");

			jogosNaTabela.add(new InformacoesDoJogo(nome, genero, modo, plataforma));
		}
		return jogosNaTabela;
	}

	/* Cria tabela de informacoes dos jogos */
	private TableView<InformacoesDoJogo> criarTableView() {
		TableColumn<InformacoesDoJogo, String> colunaDoJogo = new TableColumn<>("Jogo");
		colunaDoJogo.setCellValueFactory(new PropertyValueFactory<>("nome"));

		TableColumn<InformacoesDoJogo, String> colunaDoGenero = new TableColumn<>("Genero");
		colunaDoGenero.setCellValueFactory(new PropertyValueFactory<>("genero"));

		TableColumn<InformacoesDoJogo, String> colunaDoModo = new TableColumn<>("Modo");
		colunaDoModo.setCellValueFactory(new PropertyValueFactory<>("modo"));

		TableColumn<InformacoesDoJogo, String> colunaDaPlataforma = new TableColumn<>("Plataforma");
		colunaDaPlataforma.setCellValueFactory(new PropertyValueFactory<>("plataforma"));

		TableView<InformacoesDoJogo> tableViewJogos = new TableView<>();
		tableViewJogos.getColumns().addAll(colunaDoJogo, colunaDoGenero, colunaDoModo, colunaDaPlataforma);
		tableViewJogos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		return tableViewJogos;
	}

	public static void main(String[] args) {
		launch(args);
	}

}
