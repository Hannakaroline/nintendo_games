package nintendo;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;

public class ConsultasDBpedia {

	/**
	 * Retorna string com lista de prefixos comuns.
	 */
	private static String getPrefixes() {
		String prefixes = "";
		prefixes += "PREFIX foaf: <" + FOAF.NAMESPACE + "> \n";
		prefixes += "PREFIX rdf: <" + RDF.NAMESPACE + "> \n";
		prefixes += "PREFIX rdfs: <" + RDFS.NAMESPACE + "> \n";
		prefixes += "PREFIX dbo: <http://dbpedia.org/ontology/> \n";
		prefixes += "PREFIX dbr: <http://dbpedia.org/resource/> \n";
		prefixes += "PREFIX dct: <http://purl.org/dc/terms/> \n";
		prefixes += "PREFIX dbc: <http://dbpedia.org/resource/Category:> \n";
		return prefixes;
	}

	/* Retorna a lista de binding sets das series da franquia da nintendo */
	public static List<BindingSet> getSeriesNintendo() {
		Repository repo = new SPARQLRepository("http://dbpedia.org/sparql");
		repo.init();

		try (RepositoryConnection conn = repo.getConnection()) {
			String queryString = getPrefixes();

			queryString += "SELECT DISTINCT ?Series WHERE {";
			queryString += "	?Series dct:subject dbc:Nintendo_franchises .";
			queryString += "}";

			TupleQuery query = conn.prepareTupleQuery(queryString);

			try (TupleQueryResult result = query.evaluate()) {
				return QueryResults.asList(result);
			}
		} finally {
			repo.shutDown();
		}
	}

	/* Retorna a lista de informações dos jogos da serie passada como parametro */
	public static List<BindingSet> getInfoGame(String game) {
		Repository repo = new SPARQLRepository("http://dbpedia.org/sparql");
		repo.init();

		try (RepositoryConnection conn = repo.getConnection()) {
			String queryString = "";

			queryString += "SELECT DISTINCT ?jogo ?genero ?modo ?plataforma WHERE { ";
			queryString += "	?jogo rdf:type dbo:VideoGame .";
			queryString += "	OPTIONAL { ?jogo dbo:genre ?genre . } ";
			queryString += " 	OPTIONAL { ?jogo dbp:modes ?mode . } ";
			queryString += "	OPTIONAL { ?jogo dbo:computingPlatform ?platform . } ";
			queryString += "	?jogo dbo:series <" + game + "> .";
			queryString += "	BIND(STR(?genre) AS ?genero)";
			queryString += "	BIND(STR(?mode) AS ?modo)";
			queryString += "	BIND(STR(?platform) AS ?plataforma)";
			queryString += "}";

			TupleQuery query = conn.prepareTupleQuery(queryString);

			try (TupleQueryResult result = query.evaluate()) {
				List<BindingSet> listResult = QueryResults.asList(result);
				return listResult;
			} catch (Exception e) {
				return new ArrayList<BindingSet>();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			repo.shutDown();
		}
	}
}
