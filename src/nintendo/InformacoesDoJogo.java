package nintendo;

public class InformacoesDoJogo {
	private String nome;
	private String genero;
	private String modo;
	private String plataforma;
	
	public InformacoesDoJogo(String nome, String genero, String modo, String plataforma) {
		this.nome = nome;
		this.genero = genero;
		this.modo = modo;
		this.plataforma = plataforma;
	}
	
	public String getNome() {
		return nome;
	}
	public String getGenero() {
		return genero;
	}
	public String getModo() {
		return modo;
	}
	public String getPlataforma() {
		return plataforma;
	}
}